name "licensebork"

license "MIT"
maintainer "Richard Clamp"

homepage "http://localhost"

install_dir "/opt/licensebork"


build_version "0.0.1"
build_iteration 0

dependency "ast-gem"

exclude "\.git*"
