name "helloworld"

license "MIT"
maintainer "Richard Clamp"

homepage "http://localhost"

install_dir "/opt/helloworld"


build_version "0.0.1"
build_iteration 0

dependency "helloworld"

exclude "\.git*"
