name 'ast-gem'
default_version '2.0.0'

dependency 'ruby'

build do
  env = with_standard_compiler_flags(with_embedded_path)

  gem 'install ast' \
      " --version '#{version}'" \
      " --bindir '#{install_dir}/embedded/bin'" \
      ' --no-ri --no-rdoc', env: env
end
