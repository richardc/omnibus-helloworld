name 'helloworld'

license 'Apache-2.0'

default_version Digest::MD5.file(__FILE__).hexdigest

build do
  mkdir "#{install_dir}/bin"

  block do
    open("#{install_dir}/bin/helloworld", 'w') do |file|
      file.print <<~EOH
      #!/bin/sh
      echo "Hello world"
      EOH
    end
  end

  command "chmod 755 #{install_dir}/bin/helloworld"
end
